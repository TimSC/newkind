/*
 * Elite - The New Kind.
 *
 * Reverse engineered from the BBC disk version of Elite.
 * Additional material by C.J.Pinder.
 *
 * The original Elite code is (C) I.Bell & D.Braben 1984.
 * This version re-engineered in C by C.J.Pinder 1999-2001.
 *
 * email: <christian@newkind.co.uk>
 *
 *
 */

#include <stdlib.h>

#include "config.h"
#include "elite.h"
#include "vector.h"
#include "planet.h"
#include "shipdata.h"


struct galaxy_seed docked_planet;

struct galaxy_seed hyperspace_planet;

struct planet_data current_planet_data;

int curr_galaxy_num = 1;
int curr_fuel = 70;
int carry_flag = 0;
int current_screen = 0;
int witchspace = 0;

int config_fullscreen = 0;
int wireframe = 0;
int anti_alias_gfx = 0;
int hoopy_casinos = 0;
int speed_cap = 75;
int instant_dock = 0;
int config_joystick_num = 0;
int config_joy_v_stick = 0;
int config_joy_v_axis = 1;
int config_joy_h_stick = 0;
int config_joy_h_axis = 0;
int config_joy_b_fire = 0;
int config_joy_b_accel = 1;
int config_joy_b_brake = 2;
int config_joy_b_jump = 3;
float config_joy_deadzone = 0.2f;

char scanner_filename[256] = "newscan.cfg";
int scanner_cx = 0;
int scanner_cy = 0;
int compass_centre_x = 0;
int compass_centre_y = 0;

int planet_render_style = 0;

int game_over = 0;
int docked = 0;
int finish = 0;
int flight_speed = 0;
float flight_roll = 0.0f;
float flight_climb = 0.0f;
int front_shield = 0;
int aft_shield = 0;
int energy = 0;
int laser_temp = 0;
int detonate_bomb = 0;
int auto_pilot = 0;
int hyper_range = 70; /* Lightyears * 10 */

struct commander saved_cmdr =
{
	"JAMESON",									/* Name 			*/
	0,											/* Mission Number 	*/
	0x14,0xAD,									/* Ship X,Y			*/
	{0x4a, 0x5a, 0x48, 0x02, 0x53, 0xb7},		/* Galaxy Seed		*/
	1000,										/* Credits * 10		*/
	70,											/* Fuel	* 10		*/
	0,
	0,											/* Galaxy - 1		*/
	PULSE_LASER,								/* Front Laser		*/
	0,											/* Rear Laser		*/
	0,											/* Left Laser		*/
	0,											/* Right Laser		*/
	0, 0,
	20,											/* Cargo Capacity	*/
	{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},		/* Current Cargo	*/
	0,											/* ECM				*/
	0,											/* Fuel Scoop		*/
	0,											/* Energy Bomb		*/
	0,											/* Energy Unit		*/
	0,											/* Docking Computer */
	0,											/* Galactic H'Drive	*/
	0,											/* Escape Pod		*/
	0,0,0,0,
	3,											/* No. of Missiles	*/
	0,											/* Legal Status		*/
	{0x10, 0x0F, 0x11, 0x00, 0x03, 0x1C,		/* Station Stock	*/
	 0x0E, 0x00, 0x00, 0x0A, 0x00, 0x11,
	 0x3A, 0x07, 0x09, 0x08, 0x00},
	0,											/* Fluctuation		*/
	0,											/* Score			*/
	0x80										/* Saved			*/
};

struct commander cmdr;

struct player_ship myship;

char *default_config_filename = "newkind.cfg";
char **config_filename = &default_config_filename;

struct ship_data *ship_list[NO_OF_SHIPS + 1] =
{
	NULL,
	&missile_data,
	&coriolis_data,
	&esccaps_data,
	&alloy_data,
	&cargo_data,
	&boulder_data,
	&asteroid_data,
	&rock_data,
	&orbit_data,
	&transp_data,
	&cobra3a_data,
	&pythona_data,
	&boa_data,
	&anacnda_data,
	&hermit_data,
	&viper_data,
	&sidewnd_data,
	&mamba_data,
	&krait_data,
	&adder_data,
	&gecko_data,
	&cobra1_data,
	&worm_data,
	&cobra3b_data,
	&asp2_data,
	&pythonb_data,
	&ferdlce_data,
	&moray_data,
	&thargoid_data,
	&thargon_data,
	&constrct_data,
	&cougar_data,
	&dodec_data
};



void restore_saved_commander (void)
{
	cmdr = saved_cmdr;

	docked_planet = find_planet (cmdr.ship_x, cmdr.ship_y);
	hyperspace_planet = docked_planet;

	generate_planet_data (&current_planet_data, docked_planet);
	generate_stock_market ();
	set_stock_quantities (cmdr.station_stock);
}

void get_ship_type_name(int type, char *out)
{
	//Name is up to 32 characters
	if(type > 0 && type < NO_OF_SHIPS+1)
	{
		const struct ship_data *sd = ship_list[type];
		strncpy(out, sd->name, 32);
	}
	else if (type == 0)
	{
		strncpy(out, "Empty slot", 32);
	}
	else if (type == SHIP_PLANET)
	{
		strncpy(out, "SHIP_PLANET", 32);
	}
	else if (type == SHIP_SUN)
	{
		strncpy(out, "SHIP_SUN", 32);
	}
}

