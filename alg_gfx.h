/*
 * Elite - The New Kind.
 *
 * Reverse engineered from the BBC disk version of Elite.
 * Additional material by C.J.Pinder.
 *
 * The original Elite code is (C) I.Bell & D.Braben 1984.
 * This version re-engineered in C by C.J.Pinder 1999-2001.
 *
 * email: <christian@newkind.co.uk>
 *
 *
 */


/**
 *
 * Elite - The New Kind.
 *
 * The code in this file has not been derived from the original Elite code.
 * Written by C.J.Pinder 1999/2000.
 *
 **/


#ifndef GFX_H
#define GFX_H

#include "allegro5/allegro.h"
#include "allegro5/allegro_font.h"

#ifdef RES_800_600

#define GFX_SCALE		(2)
extern int GFX_X_OFFSET;
extern int GFX_Y_OFFSET;
#define GFX_X_CENTRE	(256)
#define GFX_Y_CENTRE	(192)

extern int GFX_VIEW_WX;
extern int GFX_VIEW_WY;
extern int GFX_CONSOLE_Y;
extern int GFX_SPACE_OX;
extern int GFX_SPACE_OY;
extern int GFX_SPACE_WX;
extern int GFX_SPACE_WY;

#define SCALE_X(X) (((X) * GFX_VIEW_WX) / 512)
#define SCALE_Y(Y) (((Y) * GFX_VIEW_WY) / 512)

#endif
 
#define GFX_COL_BLACK_TUPLE			0, 0, 0 //0
#define GFX_COL_DARK_RED_TUPLE		39*4, 0, 0 //28
#define GFX_COL_WHITE_TUPLE			63*4, 63*4, 63*4 //255
#define GFX_COL_GOLD_TUPLE			55*4, 45*4, 0 //39
#define GFX_COL_RED_TUPLE			51*4, 0, 0 //49
#define GFX_COL_CYAN_TUPLE			0, 63*4, 51*4 //11

#define GFX_COL_GREY_1_TUPLE		32*4, 32*4, 32*4 //248
#define GFX_COL_GREY_2_TUPLE		29*4, 29*4, 29*4 //235
#define GFX_COL_GREY_3_TUPLE		23*4, 23*4, 23*4 //234
#define GFX_COL_GREY_4_TUPLE		37*4, 37*4, 37*4 //237

#define GFX_COL_BLUE_1_TUPLE		0, 18*4, 40*4 //45
#define GFX_COL_BLUE_2_TUPLE		0, 0, 35*4 //46
#define GFX_COL_BLUE_3_TUPLE		0, 0, 38*4 //133
#define GFX_COL_BLUE_4_TUPLE		0, 0, 32*4 //4

#define GFX_COL_RED_3_TUPLE			32*4, 0, 0 //1
#define GFX_COL_RED_4_TUPLE			63*4, 12*4, 12*4 //71

#define GFX_COL_WHITE_2_TUPLE		56*4, 56*4, 56*4 //242

#define GFX_COL_YELLOW_1_TUPLE		51*4, 39*4, 0 //37
#define GFX_COL_YELLOW_2_TUPLE		55*4, 45*4, 0 //39
#define GFX_COL_YELLOW_3_TUPLE		63*4, 51*4, 12*4 //89
#define GFX_COL_YELLOW_4_TUPLE		63*4, 63*4, 38*4 //160
#define GFX_COL_YELLOW_5_TUPLE		63*4, 63*4, 0 //251

#define GFX_ORANGE_1_TUPLE			51*4, 25*4, 12*4 //76
#define GFX_ORANGE_2_TUPLE			63*4, 25*4, 12*4 //77
#define GFX_ORANGE_3_TUPLE			63*4, 51*4, 25*4 //122

#define GFX_COL_GREEN_1_TUPLE		0, 32*4, 0 //2
#define GFX_COL_GREEN_2_TUPLE		24*4, 43*4, 0 //17
#define GFX_COL_GREEN_3_TUPLE		25*4, 51*4, 12*4 //86

#define GFX_COL_PINK_1_TUPLE		63*4, 38*4, 51*4 //183

extern ALLEGRO_COLOR palette[29];

#define GFX_COL_BLACK		palette[0]
#define GFX_COL_DARK_RED	palette[1]
#define GFX_COL_WHITE		palette[2]
#define GFX_COL_GOLD		palette[3]
#define GFX_COL_RED			palette[4]
#define GFX_COL_CYAN		palette[5]

#define GFX_COL_GREY_1		palette[6]
#define GFX_COL_GREY_2		palette[7]
#define GFX_COL_GREY_3		palette[8]
#define GFX_COL_GREY_4		palette[9]

#define GFX_COL_BLUE_1		palette[10]
#define GFX_COL_BLUE_2		palette[11]
#define GFX_COL_BLUE_3		palette[12]
#define GFX_COL_BLUE_4		palette[13]

#define GFX_COL_RED_3		palette[14]
#define GFX_COL_RED_4		palette[15]

#define GFX_COL_WHITE_2		palette[16]

#define GFX_COL_YELLOW_1	palette[17]
#define GFX_COL_YELLOW_2	palette[18]
#define GFX_COL_YELLOW_3	palette[19]
#define GFX_COL_YELLOW_4	palette[20]
#define GFX_COL_YELLOW_5	palette[21]

#define GFX_ORANGE_1		palette[22]
#define GFX_ORANGE_2		palette[23]
#define GFX_ORANGE_3		palette[24]

#define GFX_COL_GREEN_1		palette[25]
#define GFX_COL_GREEN_2		palette[26]
#define GFX_COL_GREEN_3		palette[27]

#define GFX_COL_PINK_1		palette[28]

#define IMG_GREEN_DOT		1
#define IMG_RED_DOT			2
#define IMG_BIG_S			3
#define IMG_ELITE_TXT		4
#define IMG_BIG_E			5
#define IMG_DICE			6
#define IMG_MISSILE_GREEN	7
#define IMG_MISSILE_YELLOW	8
#define IMG_MISSILE_RED		9
#define IMG_BLAKE			10


int gfx_graphics_startup (void);
ALLEGRO_FONT *gfx_rescale_font(ALLEGRO_BITMAP *bm);
void gfx_draw_cockpit();
void gfx_set_target_backbuffer();
void gfx_graphics_shutdown (void);
void gfx_update_screen (void);
void gfx_handle_display_events(ALLEGRO_EVENT event);
void gfx_calc_screen_geometry(int w, int h);

void gfx_plot_pixel (int x, int y, ALLEGRO_COLOR col);
void gfx_fast_plot_pixel (int x, int y, ALLEGRO_COLOR col);
void gfx_draw_filled_circle (int cx, int cy, int radius, ALLEGRO_COLOR circle_colour);
void gfx_draw_circle (int cx, int cy, int radius, ALLEGRO_COLOR circle_colour);
void gfx_draw_line (int x1, int y1, int x2, int y2);
void gfx_draw_colour_line (int x1, int y1, int x2, int y2, ALLEGRO_COLOR line_colour);
void gfx_draw_triangle (int x1, int y1, int x2, int y2, int x3, int y3, ALLEGRO_COLOR col);
void gfx_draw_rectangle (int tx, int ty, int bx, int by, ALLEGRO_COLOR col);
void gfx_display_text (int x, int y, char *txt);
void gfx_display_colour_text (int x, int y, char *txt, ALLEGRO_COLOR col);
void gfx_display_centre_text (int y, char *str, int psize, ALLEGRO_COLOR col);
void gfx_clear_display (void);
void gfx_clear_text_area (void);
void gfx_clear_area (int tx, int ty, int bx, int by);
void gfx_display_pretty_text (int tx, int ty, int bx, int by, char *txt);
void gfx_draw_scanner (void);
void gfx_set_clip_region (int tx, int ty, int bx, int by);
void gfx_polygon (int num_points, int *poly_list, float *tex_list, ALLEGRO_COLOR face_colour, ALLEGRO_BITMAP* texture);
void gfx_polygon_f (int num_points, float *poly_list, float *tex_list, ALLEGRO_COLOR face_colour, ALLEGRO_BITMAP* texture);
void gfx_draw_sprite (int sprite_no, int x, int y);
void gfx_start_render (void);
void gfx_render_polygon (int num_points, int *point_list, ALLEGRO_COLOR face_colour, int zavg);
void gfx_render_line (int x1, int y1, int x2, int y2, int dist, ALLEGRO_COLOR col);
void gfx_finish_render (void);
bool gfx_request_file (char *title, char *path, char *ext, bool is_save);
void draw_laser_sights();

#endif
