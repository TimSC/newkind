
#ifndef MAIN_H
#define MAIN_H

#include "allegro5/allegro.h"

extern ALLEGRO_TIMER *timer;
extern ALLEGRO_EVENT_QUEUE *event_queue;
extern int mycount;

void info_message (char *message);

#endif
