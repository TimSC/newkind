/**
 *
 * Elite - The New Kind.
 *
 * Allegro version of Graphics routines.
 *
 * The code in this file has not been derived from the original Elite code.
 * Written by C.J.Pinder 1999-2001.
 * email: <christian@newkind.co.uk>
 *
 * Routines for drawing anti-aliased lines and circles by T.Harte.
 *
 **/

#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <ctype.h>

#include "allegro5/allegro.h"
#include "allegro5/allegro_font.h"
#include <allegro5/allegro_primitives.h>
#include <allegro5/allegro_native_dialog.h>

#include "config.h"
#include "alg_gfx.h"
#include "alg_data.h"
#include "elite.h"
#include "alg_main.h"

ALLEGRO_DISPLAY *display = NULL;
ALLEGRO_BITMAP *gfx_screen = NULL;
ALLEGRO_BITMAP *resources[14];
ALLEGRO_FONT *fonts[14];
char *res_paths[] = {"data/blake.bmp", "data/danube.mid", "data/ecm.bmp", "data/verd2.png", "data/verd4.png", 
	"data/elitetx3.bmp", "data/front.bmp", "data/greendot.bmp", "data/missgrn.bmp", 
	"data/missred.bmp", "data/missyell.bmp", "data/reddot.bmp", "data/safe.bmp", "data/theme.mid"};

ALLEGRO_BITMAP *scanner_image = NULL;
ALLEGRO_COLOR palette[29];

#define MAX_POLYS	100
#define MAX_VERTS	16

int GFX_X_OFFSET = 144;
int GFX_Y_OFFSET = 44;
int GFX_VIEW_WX = 512;
int GFX_VIEW_WY = 512;
int GFX_CONSOLE_Y = 384;

int GFX_SPACE_OX = 144;
int GFX_SPACE_OY = 44;
int GFX_SPACE_WX = 512;
int GFX_SPACE_WY = 512;

static int start_poly;
static int total_polys;

struct poly_data
{
	int z;
	int no_points;
	ALLEGRO_COLOR face_colour;
	int point_list[MAX_VERTS];
	int next;
};

static struct poly_data poly_chain[MAX_POLYS];
static ALLEGRO_VERTEX poly_vert[MAX_VERTS];

int gfx_graphics_startup (void)
{
	int display_flags = ALLEGRO_RESIZABLE;
	if(config_fullscreen)
		display_flags |= ALLEGRO_FULLSCREEN;
	al_set_new_display_flags(display_flags);

	if(anti_alias_gfx)
	{
		al_set_new_display_option(ALLEGRO_SAMPLE_BUFFERS, 1, ALLEGRO_SUGGEST);
		al_set_new_display_option(ALLEGRO_SAMPLES, 8, ALLEGRO_SUGGEST);
	}

	display = al_create_display(800, 600);
	if(!display) 
	{
		printf("failed to create display!\n");
		return 1;
	}
	al_register_event_source(event_queue, al_get_display_event_source(display));
	if(config_fullscreen)
		al_hide_mouse_cursor(display);

	for(int i=0; i<=THEME; i++)
	{
		resources[i] = NULL;
		fonts[i] = NULL;
		if(i == DANUBE || i == THEME) continue;
		
		if(i == ELITE_1 || i == ELITE_2)
		{
			resources[i] = al_load_bitmap(res_paths[i]);

			if (!resources[i])
			{
				printf("Error reading font file %s.\n", res_paths[i]);
				return 1;
			}

			fonts[i] = gfx_rescale_font(resources[i]);

			if (!fonts[i])
			{
				printf("Error reading font file %s.\n", res_paths[i]);
				return 1;
			}

		}
		else
		{
			resources[i] = al_load_bitmap(res_paths[i]);
			if (!resources[i])
			{
				printf("Error reading bitmap file %s.\n", res_paths[i]);
				return 1;
			}
		}
	}

	scanner_image = al_load_bitmap(scanner_filename);
	if (!scanner_image)
	{
		printf("Error reading scanner bitmap file.\n");
		return 1;
	}

	/* precalculate colours */
	palette[0] = al_map_rgb(GFX_COL_BLACK_TUPLE);
	palette[1] = al_map_rgb(GFX_COL_DARK_RED_TUPLE);
	palette[2] = al_map_rgb(GFX_COL_WHITE_TUPLE);
	palette[3] = al_map_rgb(GFX_COL_GOLD_TUPLE);
	palette[4] = al_map_rgb(GFX_COL_RED_TUPLE);
	palette[5] = al_map_rgb(GFX_COL_CYAN_TUPLE);

	palette[6] = al_map_rgb(GFX_COL_GREY_1_TUPLE);
	palette[7] = al_map_rgb(GFX_COL_GREY_2_TUPLE);
	palette[8] = al_map_rgb(GFX_COL_GREY_3_TUPLE);
	palette[9] = al_map_rgb(GFX_COL_GREY_4_TUPLE);

	palette[10] = al_map_rgb(GFX_COL_BLUE_1_TUPLE);
	palette[11] = al_map_rgb(GFX_COL_BLUE_2_TUPLE);
	palette[12] = al_map_rgb(GFX_COL_BLUE_3_TUPLE);
	palette[13] = al_map_rgb(GFX_COL_BLUE_4_TUPLE);

	palette[14] = al_map_rgb(GFX_COL_RED_3_TUPLE);
	palette[15] = al_map_rgb(GFX_COL_RED_4_TUPLE);

	palette[16] = al_map_rgb(GFX_COL_WHITE_2_TUPLE);

	palette[17] = al_map_rgb(GFX_COL_YELLOW_1_TUPLE);
	palette[18] = al_map_rgb(GFX_COL_YELLOW_2_TUPLE);
	palette[19] = al_map_rgb(GFX_COL_YELLOW_3_TUPLE);
	palette[20] = al_map_rgb(GFX_COL_YELLOW_4_TUPLE);
	palette[21] = al_map_rgb(GFX_COL_YELLOW_5_TUPLE);

	palette[22] = al_map_rgb(GFX_ORANGE_1_TUPLE);
	palette[23] = al_map_rgb(GFX_ORANGE_2_TUPLE);
	palette[24] = al_map_rgb(GFX_ORANGE_3_TUPLE);

	palette[25] = al_map_rgb(GFX_COL_GREEN_1_TUPLE);
	palette[26] = al_map_rgb(GFX_COL_GREEN_2_TUPLE);
	palette[27] = al_map_rgb(GFX_COL_GREEN_3_TUPLE);

	palette[28] = al_map_rgb(GFX_COL_PINK_1_TUPLE);

	gfx_calc_screen_geometry(800, 600);

	/* Create the screen buffer bitmap */
	gfx_screen = al_get_backbuffer(display);

	al_set_target_bitmap(gfx_screen);
	al_clear_to_color(al_map_rgb(0, 0, 0));
	al_set_blender(ALLEGRO_ADD, ALLEGRO_ONE, ALLEGRO_INVERSE_ALPHA);

	return 0;
}

ALLEGRO_FONT *gfx_rescale_font(ALLEGRO_BITMAP *bm)
{
	int new_width = SCALE_X(al_get_bitmap_width(bm));
	int new_height = SCALE_Y(al_get_bitmap_height(bm));
	al_set_new_bitmap_flags(al_get_bitmap_flags(bm));
	al_set_new_bitmap_format(al_get_bitmap_format(bm));
	ALLEGRO_BITMAP *tmp = al_create_bitmap(new_width, new_height);

	al_set_target_bitmap(tmp);
	al_set_blender(ALLEGRO_SRC_MINUS_DEST, ALLEGRO_ALPHA, ALLEGRO_ZERO);
	al_draw_scaled_bitmap(bm,
		0, 0, al_get_bitmap_width(bm), al_get_bitmap_height(bm),
		0, 0, new_width, new_height, 0);

	int range[2];
	range[0] = 32;
	range[1] = 32 + 94;
	ALLEGRO_FONT *out = al_grab_font_from_bitmap(tmp, 1, range);

	al_destroy_bitmap(tmp);
	return out;
}

void gfx_draw_cockpit()
{
	gfx_draw_line (0, 0, 0, GFX_CONSOLE_Y);
	gfx_draw_line (0, 0, GFX_VIEW_WX-1, 0);
	gfx_draw_line (GFX_VIEW_WX-1, 0, GFX_VIEW_WX-1, GFX_CONSOLE_Y);
}

void gfx_set_target_backbuffer()
{
	al_set_target_bitmap(gfx_screen);
}

void gfx_graphics_shutdown (void)
{
	al_destroy_bitmap(scanner_image);
	for(int i=0; i<=THEME; i++)
	{
		if(resources[i] != NULL)
			al_destroy_bitmap(resources[i]);
		if(fonts[i] != NULL)
			al_destroy_font(fonts[i]);
	}
	al_destroy_display(display);
}

/*
 * Blit the back buffer to the screen.
 */

void gfx_update_screen (void)
{
	al_flip_display();	
}

void gfx_calc_screen_geometry(int w, int h)
{
	if(1)
	{
		//Zoom display
		//Scale drawing area be as large as possible
		//while remaining a square.
		if(h < w)
			GFX_VIEW_WX = h;
		else
			GFX_VIEW_WX = w;
	}
	else
	{
		//Classic screen resolution
		GFX_VIEW_WX = 512;
	}
	GFX_VIEW_WY = GFX_VIEW_WX;
	GFX_CONSOLE_Y = GFX_VIEW_WY * 0.75;

	//Calculate padding around drawing area
	GFX_X_OFFSET = (w - GFX_VIEW_WX)/2;
	GFX_Y_OFFSET = (h - GFX_VIEW_WY)/2;

	if(0)
	{
		//Square space view
		GFX_SPACE_OX = GFX_X_OFFSET;
		GFX_SPACE_OY = GFX_Y_OFFSET;
		GFX_SPACE_WX = GFX_VIEW_WX;
		GFX_SPACE_WY = GFX_CONSOLE_Y;
	}
	else
	{
		//Wide space view
		GFX_SPACE_OX = 0;
		GFX_SPACE_OY = 0;
		GFX_SPACE_WX = w;
		GFX_SPACE_WY = h;
	}

	//Resize fonts
	if(fonts[ELITE_1]) al_destroy_font(fonts[ELITE_1]);
	fonts[ELITE_1] = gfx_rescale_font(resources[ELITE_1]);
	if(fonts[ELITE_2]) al_destroy_font(fonts[ELITE_2]);
	fonts[ELITE_2] = gfx_rescale_font(resources[ELITE_2]);

	//Restore render settings
	al_set_blender(ALLEGRO_ADD, ALLEGRO_ONE, ALLEGRO_INVERSE_ALPHA);
}

void gfx_handle_display_events(ALLEGRO_EVENT event)
{
	if(event.type == ALLEGRO_EVENT_DISPLAY_RESIZE)
	{
		int width = event.display.width;
		int height = event.display.height;

		gfx_calc_screen_geometry(width, height);

		al_acknowledge_resize(display);
	}

	if(event.type == ALLEGRO_EVENT_DISPLAY_CLOSE)
	{
		game_over = 1;
		finish = 1;
	}
}

void gfx_fast_plot_pixel (int x, int y, ALLEGRO_COLOR col)
{
	al_put_pixel(x, y, col);
}

void gfx_plot_pixel (int x, int y, ALLEGRO_COLOR col)
{
	al_put_pixel(x + GFX_X_OFFSET, y + GFX_Y_OFFSET, col);
}


void gfx_draw_filled_circle (int cx, int cy, int radius, ALLEGRO_COLOR circle_colour)
{
	al_draw_filled_circle(cx + GFX_X_OFFSET + 0.5f, cy + GFX_Y_OFFSET + 0.5f, radius, circle_colour);
}


void gfx_draw_circle (int cx, int cy, int radius, ALLEGRO_COLOR circle_colour)
{
	al_draw_circle(cx + GFX_X_OFFSET + 0.5f, cy + GFX_Y_OFFSET + 0.5f, radius, circle_colour, 1.f);
}



void gfx_draw_line (int x1, int y1, int x2, int y2)
{
	al_draw_line(x1 + GFX_X_OFFSET + 0.5f, y1 + GFX_Y_OFFSET + 0.5f, x2 + GFX_X_OFFSET + 0.5f, y2 + GFX_Y_OFFSET + 0.5f, GFX_COL_WHITE, 1.f);
}



void gfx_draw_colour_line (int x1, int y1, int x2, int y2, ALLEGRO_COLOR line_colour)
{
	al_draw_line(x1 + GFX_X_OFFSET + 0.5f, y1 + GFX_Y_OFFSET + 0.5f, x2 + GFX_X_OFFSET + 0.5f, y2 + GFX_Y_OFFSET + 0.5f, line_colour, 1.f);
}



void gfx_draw_triangle (int x1, int y1, int x2, int y2, int x3, int y3, ALLEGRO_COLOR col)
{
	al_draw_filled_triangle(x1 + GFX_X_OFFSET, y1 + GFX_Y_OFFSET, x2 + GFX_X_OFFSET, y2 + GFX_Y_OFFSET,
		x3 + GFX_X_OFFSET, y3 + GFX_Y_OFFSET, col);
}



void gfx_display_text (int x, int y, char *txt)
{
	al_draw_text(fonts[ELITE_1], GFX_COL_WHITE, 
		SCALE_X(x / (2 / GFX_SCALE)) + GFX_X_OFFSET, SCALE_Y(y / (2 / GFX_SCALE)) + GFX_Y_OFFSET, 0, txt);
}


void gfx_display_colour_text (int x, int y, char *txt, ALLEGRO_COLOR col)
{
	al_draw_text(fonts[ELITE_1], col, 
		SCALE_X(x / (2 / GFX_SCALE)) + GFX_X_OFFSET, SCALE_Y(y / (2 / GFX_SCALE)) + GFX_Y_OFFSET, 0, txt);
}



void gfx_display_centre_text (int y, char *str, int psize, ALLEGRO_COLOR col)
{
	int txt_size;
	ALLEGRO_COLOR txt_colour;
	
	if (psize == 140)
	{
		txt_size = ELITE_2;
		txt_colour = al_map_rgb(255, 255, 255);
	}
	else
	{
		txt_size = ELITE_1;
		txt_colour = col;
	}

	al_draw_text(fonts[txt_size], txt_colour, 
		SCALE_X(128 * GFX_SCALE) + GFX_X_OFFSET, 
		SCALE_Y(y / (2 / GFX_SCALE)) + GFX_Y_OFFSET, ALLEGRO_ALIGN_CENTRE, str);
}


void gfx_clear_display (void)
{
	al_draw_filled_rectangle(GFX_SPACE_OX, GFX_SPACE_OY, GFX_SPACE_OX+GFX_SPACE_WX, GFX_SPACE_OY+GFX_SPACE_WY, GFX_COL_BLACK);
}

void gfx_clear_text_area (void)
{
	al_draw_filled_rectangle(GFX_X_OFFSET + 1, GFX_Y_OFFSET + SCALE_Y(340), (GFX_VIEW_WX-2) + GFX_X_OFFSET + 1.f, GFX_CONSOLE_Y + GFX_Y_OFFSET, GFX_COL_BLACK);
}


void gfx_clear_area (int tx, int ty, int bx, int by)
{
	al_draw_filled_rectangle(tx + GFX_X_OFFSET, ty + GFX_Y_OFFSET, bx + GFX_X_OFFSET + 1.f, by + GFX_Y_OFFSET + 1.f, GFX_COL_BLACK);
}

void gfx_draw_rectangle (int tx, int ty, int bx, int by, ALLEGRO_COLOR col)
{
	al_draw_filled_rectangle(tx + GFX_X_OFFSET, ty + GFX_Y_OFFSET, bx + GFX_X_OFFSET + 1.f, by + GFX_Y_OFFSET + 1.f, col);
}


void gfx_display_pretty_text (int tx, int ty, int bx, int by, char *txt)
{
	char strbuf[100];
	char *str;
	char *bptr;
	int len;
	int pos;
	int maxlen;
	
	maxlen = (bx - tx) / 8;

	str = txt;	
	len = strlen(txt);
	
	while (len > 0)
	{
		pos = maxlen;
		if (pos > len)
			pos = len;

		while ((str[pos] != ' ') && (str[pos] != ',') &&
			   (str[pos] != '.') && (str[pos] != '\0'))
		{
			pos--;
		}

		len = len - pos - 1;
	
		for (bptr = strbuf; pos >= 0; pos--)
			*bptr++ = *str++;

		*bptr = '\0';

		al_draw_text(fonts[ELITE_1], GFX_COL_WHITE, SCALE_X(tx) + GFX_X_OFFSET, SCALE_Y(ty) + GFX_Y_OFFSET, 0, strbuf);
		ty += (8 * GFX_SCALE);
	}
}


void gfx_draw_scanner (void)
{
	int dst_height = GFX_CONSOLE_Y+1;

	al_draw_scaled_bitmap(scanner_image,
	   0, 0, al_get_bitmap_width(scanner_image), al_get_bitmap_height(scanner_image),
	   GFX_X_OFFSET, dst_height+GFX_Y_OFFSET, GFX_VIEW_WX, GFX_VIEW_WY-dst_height, 0);
}

void gfx_set_clip_region (int tx, int ty, int bx, int by)
{
	al_set_clipping_rectangle(tx + GFX_X_OFFSET, ty + GFX_Y_OFFSET, bx-tx, by-ty);
}


void gfx_start_render (void)
{
	start_poly = 0;
	total_polys = 0;
}

void gfx_render_polygon (int num_points, int *point_list, ALLEGRO_COLOR face_colour, int zavg)
{
	int i;
	int x;
	int nx;
	
	if (total_polys == MAX_POLYS)
		return;

	x = total_polys;
	total_polys++;
	
	poly_chain[x].no_points = num_points;
	poly_chain[x].face_colour = face_colour;
	poly_chain[x].z = zavg;
	poly_chain[x].next = -1;

	for (i = 0; i < MAX_VERTS; i++)
	{
		poly_chain[x].point_list[i] = point_list[i];
	}

	if (x == 0)
		return;

	if (zavg > poly_chain[start_poly].z)
	{
		poly_chain[x].next = start_poly;
		start_poly = x;
		return;
	} 	

	for (i = start_poly; poly_chain[i].next != -1; i = poly_chain[i].next)
	{
		nx = poly_chain[i].next;
		
		if (zavg > poly_chain[nx].z)
		{
			poly_chain[i].next = x;
			poly_chain[x].next = nx;
			return;
		}
	}	
	
	poly_chain[i].next = x;
}


void gfx_render_line (int x1, int y1, int x2, int y2, int dist, ALLEGRO_COLOR col)
{
	int point_list[4];
	
	point_list[0] = x1;
	point_list[1] = y1;
	point_list[2] = x2;
	point_list[3] = y2;
	
	gfx_render_polygon (2, point_list, col, dist);
}


void gfx_finish_render (void)
{
	int num_points;
	int *pl;
	int i;
	ALLEGRO_COLOR col;
	
	if (total_polys == 0)
		return;
		
	for (i = start_poly; i != -1; i = poly_chain[i].next)
	{
		num_points = poly_chain[i].no_points;
		pl = poly_chain[i].point_list;
		col = poly_chain[i].face_colour;

		if (num_points == 2)
		{
			gfx_draw_colour_line (pl[0], pl[1], pl[2], pl[3], col);
			continue;
		}
		
		gfx_polygon (num_points, pl, NULL, col, NULL); 
	};
}

void gfx_polygon (int num_points, int *poly_list, float *tex_list, ALLEGRO_COLOR face_colour, ALLEGRO_BITMAP* texture)
{
	float poly_list_f[MAX_VERTS];
	for(int i=0; i<num_points*2; i++)
	{
		poly_list_f[i] = poly_list[i];
	}	
	gfx_polygon_f (num_points, poly_list_f, tex_list, face_colour, texture);
}

void gfx_polygon_f (int num_points, float *poly_list, float *tex_list, ALLEGRO_COLOR face_colour, ALLEGRO_BITMAP* texture)
{
	int i;
	int x,y;
	
	x = 0;
	y = 1;
	for (i = 0; i < num_points; i++)
	{
		poly_vert[i].x = poly_list[x] + GFX_X_OFFSET;
		poly_vert[i].y = poly_list[y] + GFX_Y_OFFSET;
		poly_vert[i].z = 0;
		poly_vert[i].color = face_colour;
		if(tex_list != NULL)
		{
			poly_vert[i].u = tex_list[x];
			poly_vert[i].v = tex_list[y];
		}
		else
		{
			poly_vert[i].u = 0;
			poly_vert[i].v = 0;
		}

		x += 2;
		y += 2;

	}

	al_draw_prim(poly_vert, NULL,
	   texture, 0, num_points, ALLEGRO_PRIM_TRIANGLE_FAN);
}


void gfx_draw_sprite (int sprite_no, int x, int y)
{
	ALLEGRO_BITMAP *sprite_bmp;
	
	switch (sprite_no)
	{
		case IMG_GREEN_DOT:
			sprite_bmp = resources[GRNDOT];
			break;
		
		case IMG_RED_DOT:
			sprite_bmp = resources[REDDOT];
			break;
			
		case IMG_BIG_S:
			sprite_bmp = resources[SAFE];
			break;
		
		case IMG_ELITE_TXT:
			sprite_bmp = resources[ELITETXT];
			break;
			
		case IMG_BIG_E:
			sprite_bmp = resources[ECM];
			break;
			
		case IMG_BLAKE:
			sprite_bmp = resources[BLAKE];
			break;
		
		case IMG_MISSILE_GREEN:
			sprite_bmp = resources[MISSILE_G];
			break;

		case IMG_MISSILE_YELLOW:
			sprite_bmp = resources[MISSILE_Y];
			break;

		case IMG_MISSILE_RED:
			sprite_bmp = resources[MISSILE_R];
			break;

		default:
			return;
	}

	if (x == -1)
		x = SCALE_X(((256 * GFX_SCALE) - al_get_bitmap_width(sprite_bmp)) / 2);
	
	al_draw_scaled_bitmap(sprite_bmp, 
		0, 0, al_get_bitmap_width(sprite_bmp), al_get_bitmap_height(sprite_bmp),
		x + GFX_X_OFFSET, y + GFX_Y_OFFSET, 
		SCALE_X(al_get_bitmap_width(sprite_bmp)), SCALE_Y(al_get_bitmap_height(sprite_bmp)),
		0);
	   
}


bool gfx_request_file (char *title, char *path, char *ext, bool is_save)
{
	int okay;

	al_set_system_mouse_cursor(display, ALLEGRO_SYSTEM_MOUSE_CURSOR_DEFAULT);

	int mode = 0;
	if(is_save)
		mode |= ALLEGRO_FILECHOOSER_SAVE;
	else
		mode |= ALLEGRO_FILECHOOSER_FILE_MUST_EXIST;
	ALLEGRO_FILECHOOSER *chooser = al_create_native_file_dialog(
		path,
		title,
		ext,
		mode);

	al_stop_timer(timer);

	okay = al_show_native_file_dialog(display, chooser);

	al_start_timer(timer);

	if(okay)
	{
		int count = al_get_native_file_dialog_count(chooser);
		if(count > 0)
		{
			const char *name = al_get_native_file_dialog_path(chooser, 0);
			strncpy(path, name, 254);
		}
		else
			okay = 0;
	}

	al_destroy_native_file_dialog(chooser);

	al_hide_mouse_cursor(display);

	return okay;
}


void draw_laser_sights()
{
	int laser = 0;
	int x1,y1,x2,y2;
	
	switch (current_screen)
	{
		case SCR_FRONT_VIEW:
			gfx_display_centre_text (32, "Front View", 120, GFX_COL_WHITE);
			laser = cmdr.front_laser;
			break;
		
		case SCR_REAR_VIEW:
			gfx_display_centre_text (32, "Rear View", 120, GFX_COL_WHITE);
			laser = cmdr.rear_laser;
			break;

		case SCR_LEFT_VIEW:
			gfx_display_centre_text (32, "Left View", 120, GFX_COL_WHITE);
			laser = cmdr.left_laser;
			break;

		case SCR_RIGHT_VIEW:
			gfx_display_centre_text (32, "Right View", 120, GFX_COL_WHITE);
			laser = cmdr.right_laser;
			break;
	}
	

	if (laser)
	{
		x1 = 128 * GFX_SCALE;
		y1 = (96-8) * GFX_SCALE;
		y2 = (96-16) * GFX_SCALE;
   
		gfx_draw_rectangle (SCALE_X(x1-1), SCALE_Y(y1), SCALE_X(x1+1), SCALE_Y(y2), GFX_COL_GREY_1);
		gfx_draw_colour_line (SCALE_X(x1), SCALE_Y(y1), SCALE_X(x1), SCALE_Y(y2), GFX_COL_WHITE);

		y1 = (96+8) * GFX_SCALE;
		y2 = (96+16) * GFX_SCALE;
		
		gfx_draw_rectangle (SCALE_X(x1-1), SCALE_Y(y1), SCALE_X(x1+1), SCALE_Y(y2), GFX_COL_GREY_1);
		gfx_draw_colour_line (SCALE_X(x1), SCALE_Y(y1), SCALE_X(x1), SCALE_Y(y2), GFX_COL_WHITE);

		x1 = (128-8) * GFX_SCALE;
		y1 = 96 * GFX_SCALE;
		x2 = (128-16) * GFX_SCALE;
		  
		gfx_draw_rectangle (SCALE_X(x1), SCALE_Y(y1-1), SCALE_X(x2), SCALE_Y(y1+1), GFX_COL_GREY_1); 
		gfx_draw_colour_line (SCALE_X(x1), SCALE_Y(y1), SCALE_X(x2), SCALE_Y(y1), GFX_COL_WHITE);

		x1 = (128+8) * GFX_SCALE;
		x2 = (128+16) * GFX_SCALE;

		gfx_draw_rectangle (SCALE_X(x1), SCALE_Y(y1-1), SCALE_X(x2), SCALE_Y(y1+1), GFX_COL_GREY_1); 
		gfx_draw_colour_line (SCALE_X(x1), SCALE_Y(y1), SCALE_X(x2), SCALE_Y(y1), GFX_COL_WHITE);
	}
}

