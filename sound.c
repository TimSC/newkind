/*
 * Elite - The New Kind.
 *
 * Reverse engineered from the BBC disk version of Elite.
 * Additional material by C.J.Pinder.
 *
 * The original Elite code is (C) I.Bell & D.Braben 1984.
 * This version re-engineered in C by C.J.Pinder 1999-2001.
 *
 * email: <christian@newkind.co.uk>
 *
 *
 */

/*
 * sound.c
 */

#include <stdio.h>
#include <stdlib.h>
#include <allegro5/allegro_audio.h>
#include "sound.h"
#include "alg_data.h" 
#include "alg_main.h"

#define NUM_SAMPLES 14 

static int sound_on;

ALLEGRO_AUDIO_STREAM *music[2];
ALLEGRO_VOICE *voice = NULL;
ALLEGRO_MIXER* mixer = NULL;

struct sound_sample
{
 	ALLEGRO_SAMPLE *sample;
	char filename[256];
	int runtime;
	int timeleft;
};

struct sound_sample sample_list[NUM_SAMPLES] =
{
	{NULL, "data/launch.wav",    32, 0},
	{NULL, "data/crash.wav",      7, 0},
	{NULL, "data/dock.wav",      36, 0},
	{NULL, "data/gameover.wav",  24, 0},
	{NULL, "data/pulse.wav",      4, 0},
	{NULL, "data/hitem.wav",		 4, 0},
	{NULL, "data/explode.wav",	23, 0},
	{NULL, "data/ecm.wav",		23, 0},
	{NULL, "data/missile.wav",	25, 0},
	{NULL, "data/hyper.wav",	    37, 0},
	{NULL, "data/incom1.wav",	 4, 0},
	{NULL, "data/incom2.wav",	 5, 0},
	{NULL, "data/beep.wav",		 2, 0},
	{NULL, "data/boop.wav",		 7, 0},
};
 
 
void snd_sound_startup (void)
{
	int i;

 	/* Install a sound driver.. */
	sound_on = 1;
	music[0] = NULL;
	music[1] = NULL;
	
	if (!al_install_audio())
	{
		printf("failed to al_install_audio!\n");
		sound_on = 0;
		return;
	}

	if(!al_init_acodec_addon())
	{
		printf("failed to al_init_acodec_addon!\n");
		sound_on = 0;
		return;
	}

	voice = al_create_voice(44100, ALLEGRO_AUDIO_DEPTH_INT16, ALLEGRO_CHANNEL_CONF_2); 
	if (!voice) 
	{ 
		printf("Could not create ALLEGRO_VOICE.\n");
		sound_on = 0;
		return;
	}

	mixer = al_create_mixer(44100, ALLEGRO_AUDIO_DEPTH_INT16, ALLEGRO_CHANNEL_CONF_2); 
	if (!mixer) 
	{
		printf("Could not create ALLEGRO_MIXER.\n");
		sound_on = 0;
		return;
	}
	
	al_attach_mixer_to_voice(mixer, voice);

	if (!al_set_default_mixer(mixer))
	{
		printf("Could not set default mixer.\n");
		sound_on = 0;
		return;
	}

	if(!al_reserve_samples(NUM_SAMPLES))
	{
		printf("failed to al_reserve_samples!\n");
		sound_on = 0;
		return;
	}

	/* Load the sound samples... */

	for (i = 0; i < NUM_SAMPLES; i++)
	{
		sample_list[i].sample = al_load_sample(sample_list[i].filename);

		if (!sample_list[i].sample){
			printf( "Audio clip sample %s not loaded!\n", sample_list[i].filename); 
		}
	}

	char music0_fi[] = "data/c64-bell-elite-docking-sequence.ogg";
	music[0] = al_load_audio_stream(music0_fi, 4, 2048);
	if(music[0]==NULL)
	{
		printf("failed to load %s!\n", music0_fi);
		sound_on = 0;
		return;
	}

	char music1_fi[] = "data/c64-bell-elite-title.ogg";
	music[1] = al_load_audio_stream(music1_fi, 4, 2048);
	if(music[1]==NULL)
	{
		printf("failed to load %s!\n", music1_fi);
		sound_on = 0;
		return;
	}
}

void snd_sound_shutdown (void)
{
	int i;

	if (!sound_on)
		return;

	for (i = 0; i < NUM_SAMPLES; i++)
	{
		if (sample_list[i].sample != NULL)
		{
			al_destroy_sample (sample_list[i].sample);
			sample_list[i].sample = NULL;
		}
	}

	al_destroy_audio_stream(music[0]);
	al_destroy_audio_stream(music[1]);

	al_destroy_mixer(mixer);

	al_destroy_voice(voice);
}


void snd_play_sample (int sample_no)
{
	if (!sound_on)
		return;

	if (sample_list[sample_no].timeleft != 0)
		return;

	sample_list[sample_no].timeleft = sample_list[sample_no].runtime;
		
	al_play_sample (sample_list[sample_no].sample, 1.0f, 0.0, 1.0, ALLEGRO_PLAYMODE_ONCE, NULL);
}


void snd_update_sound (void)
{
	int i;
	
	for (i = 0; i < NUM_SAMPLES; i++)
	{
		if (sample_list[i].timeleft > 0)
			sample_list[i].timeleft--;
	}
}


void snd_play_midi (int midi_no, int repeat)
{
	if (!sound_on)
		return;

	switch (midi_no)
	{
		case SND_ELITE_THEME:
			al_seek_audio_stream_secs(music[1], 0.0);
			al_attach_audio_stream_to_mixer(music[1], mixer);
			break;
		
		case SND_BLUE_DANUBE:
			al_seek_audio_stream_secs(music[0], 0.0);
			al_attach_audio_stream_to_mixer(music[0], mixer);
			break;
	}
}


void snd_stop_midi (void)
{
	if (sound_on)
	{
		al_detach_audio_stream(music[0]);
		al_detach_audio_stream(music[1]);
	}
}
