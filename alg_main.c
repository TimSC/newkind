/*
 * Elite - The New Kind.
 *
 * Reverse engineered from the BBC disk version of Elite.
 * Additional material by C.J.Pinder.
 *
 * The original Elite code is (C) I.Bell & D.Braben 1984.
 * This version re-engineered in C by C.J.Pinder 1999-2001.
 *
 * email: <christian@newkind.co.uk>
 *
 *
 */

/*
 * alg_main.c
 *
 * Allegro version of the main game handler.
 */


#include <stdio.h>
#include <string.h>
#include <math.h> 
#include <ctype.h>
#include <time.h>
#include <stdlib.h>
#include <stdint.h>

#include "allegro5/allegro.h"

#include "config.h"
#include "alg_gfx.h"
#include "main.h"
#include "vector.h"
#include "alg_data.h"
#include "elite.h"
#include "docked.h"
#include "intro.h"
#include "shipdata.h"
#include "shipface.h"
#include "space.h"
#include "sound.h"
#include "threed.h"
#include "swat.h"
#include "random.h"
#include "options.h"
#include "stars.h"
#include "missions.h"
#include "pilot.h"
#include "file.h"
#include "keyboard.h"

ALLEGRO_EVENT_QUEUE *event_queue = NULL;
ALLEGRO_TIMER *timer = NULL;
ALLEGRO_JOYSTICK *joystick = NULL;

int old_cross_x = 0, old_cross_y = 0;
int cross_timer = 0;

int draw_lasers = 0;
int mycount = 0;
int message_count = 0;
char message_string[80] = "";
int rolling = 0;
int climbing = 0;
int game_paused = 0;
int have_joystick = 0;

int find_input = 0;
char find_name[20] = "";
bool find_unknown = false;
int64_t prev_timer_count = 0;

float joy_h = 0.0f, joy_v = 0.0f; //Post processed joystick position
bool joy_h_in_deadzone = true, joy_v_in_deadzone = true;
char save_path[255];

/*
 * Initialise the game parameters.
 */

void initialise_game(void)
{
	set_rand_seed (time(NULL));
	current_screen = SCR_INTRO_ONE;

	restore_saved_commander();

	flight_speed = 1;
	flight_roll = 0;
	flight_climb = 0;
	docked = 1;
	front_shield = 255;
	aft_shield = 255;
	energy = 255;
	draw_lasers = 0;
	mycount = 0;
	hyper_ready = 0;
	detonate_bomb = 0;
	find_input = 0;
	witchspace = 0;
	game_paused = 0;
	auto_pilot = 0;
	
	create_new_stars();
	clear_universe();
	
	cross_x = -1;
	cross_y = -1;
	cross_timer = 0;

	
	myship.max_speed = 40;		/* 0.27 Light Mach */
	myship.max_roll = 31;
	myship.max_climb = 8;		/* CF 8 */
	myship.max_fuel = 70;		/* 7.0 Light Years */
}


void finish_game (void)
{
	finish = 1;
	game_over = 1;
}







/*
 * Move the planet chart cross hairs to specified position.
 */


void move_cross (float dx, float dy)
{
	cross_timer = 5;

	if (current_screen == SCR_SHORT_RANGE)
	{
		cross_x += (dx * 4.0f);
		cross_y += (dy * 4.0f);
		return;
	}

	if (current_screen == SCR_GALACTIC_CHART)
	{
		cross_x += (dx * 2.0f);
		cross_y += (dy * 2.0f);

		if (cross_x < 1)
			cross_x = 1;
			
		if (cross_x > GFX_VIEW_WX-2)
			cross_x = GFX_VIEW_WX-2;

		if (cross_y < 37)
			cross_y = 37;
		
		if (cross_y > 293)
			cross_y = 293;
	}
}


/*
 * Draw the cross hairs at the specified position.
 */

void draw_cross (int cx, int cy)
{
	if (current_screen == SCR_SHORT_RANGE)
	{
		gfx_set_clip_region (1, SCALE_Y(37), GFX_VIEW_WX-2, SCALE_Y(339));
		gfx_draw_colour_line (SCALE_X(cx - 16), SCALE_Y(cy), SCALE_X(cx + 16), SCALE_Y(cy), GFX_COL_RED);
		gfx_draw_colour_line (SCALE_X(cx), SCALE_Y(cy - 16), SCALE_X(cx), SCALE_Y(cy + 16), GFX_COL_RED);
		gfx_set_clip_region (1, 1, GFX_VIEW_WX-2, GFX_CONSOLE_Y-1);
		return;
	}
	
	if (current_screen == SCR_GALACTIC_CHART)
	{
		gfx_set_clip_region (1, SCALE_Y(37), GFX_VIEW_WX-2, SCALE_Y(293));
		gfx_draw_colour_line (SCALE_X(cx - 8), SCALE_Y(cy), SCALE_X(cx + 8), SCALE_Y(cy), GFX_COL_RED);
		gfx_draw_colour_line (SCALE_X(cx), SCALE_Y(cy - 8), SCALE_X(cx), SCALE_Y(cy + 8), GFX_COL_RED);
		gfx_set_clip_region (1, 1, GFX_VIEW_WX-2, GFX_CONSOLE_Y-1);
	}
}



void arrow_right (float val)
{
	switch (current_screen)
	{
		case SCR_MARKET_PRICES:
			if(val > 0.5f)
				buy_stock();
			break;
		
		case SCR_SETTINGS:
			if(val > 0.5f)
				select_right_setting();
			break;

		case SCR_SHORT_RANGE:
		case SCR_GALACTIC_CHART:
			move_cross(val, 0);
			break;

		case SCR_FRONT_VIEW:
		case SCR_REAR_VIEW:
		case SCR_RIGHT_VIEW:
		case SCR_LEFT_VIEW:
			if (flight_roll > 0)
				flight_roll = 0;
			else
			{
				decrease_flight_roll(2.0f*val);
				rolling = 1;
			}
			break;
	}
}


void arrow_left (float val)
{
	switch (current_screen)
	{
		case SCR_MARKET_PRICES:
			if(val > 0.5f)
				sell_stock();
			break;

		case SCR_SETTINGS:
			if(val > 0.5f)
				select_left_setting();
			break;
		
		case SCR_SHORT_RANGE:
		case SCR_GALACTIC_CHART:
			move_cross (-val, 0);
			break;

		case SCR_FRONT_VIEW:
		case SCR_REAR_VIEW:
		case SCR_RIGHT_VIEW:
		case SCR_LEFT_VIEW:
			if (flight_roll < 0)
				flight_roll = 0;
			else
			{
				increase_flight_roll(2.0f*val);
				rolling = 1;
			}
			break;
	}
}


void arrow_up (float val)
{
	switch (current_screen)
	{
		case SCR_MARKET_PRICES:
			if(val > 0.5)
				select_previous_stock();
			break;

		case SCR_EQUIP_SHIP:
			if(val > 0.5)
				select_previous_equip();
			break;

		case SCR_OPTIONS:
			if(val > 0.5)
				select_previous_option();
			break;

		case SCR_SETTINGS:
			if(val > 0.5)
				select_up_setting();
			break;
		
		case SCR_SHORT_RANGE:
		case SCR_GALACTIC_CHART:
			move_cross (0, -val);
			break;

		case SCR_FRONT_VIEW:
		case SCR_REAR_VIEW:
		case SCR_RIGHT_VIEW:
		case SCR_LEFT_VIEW:
			if (flight_climb > 0)
				flight_climb = 0;
			else
			{
				decrease_flight_climb(1.0f*val);
			}
			climbing = 1;
			break;
	}
}



void arrow_down (float val)
{
	switch (current_screen)
	{
		case SCR_MARKET_PRICES:
			if(val > 0.5f)
				select_next_stock();
			break;

		case SCR_EQUIP_SHIP:
			if(val > 0.5f)
				select_next_equip();
			break;
		
		case SCR_OPTIONS:
			if(val > 0.5f)
				select_next_option();
			break;

		case SCR_SETTINGS:
			if(val > 0.5f)
				select_down_setting();
			break;
		
		case SCR_SHORT_RANGE:
		case SCR_GALACTIC_CHART:
			move_cross (0, val);
			break;

		case SCR_FRONT_VIEW:
		case SCR_REAR_VIEW:
		case SCR_RIGHT_VIEW:
		case SCR_LEFT_VIEW:
			if (flight_climb < 0)
				flight_climb = 0;
			else
			{
				increase_flight_climb(1.0f*val);
			}
			climbing = 1;
			break;

	}
}


void return_pressed (void)
{
	switch (current_screen)
	{
		case SCR_EQUIP_SHIP:
			buy_equip();
			break;
		
		case SCR_OPTIONS:
			do_option();
			break;

		case SCR_SETTINGS:
			toggle_setting();
			break;
	}	
}


void y_pressed (void)
{
	switch (current_screen)
	{
		case SCR_QUIT:
			finish_game();
			break;
	}
}


void n_pressed (void)
{
	switch (current_screen)
	{
		case SCR_QUIT:
			if (docked)
				current_screen = SCR_CMDR_STATUS;
			else
				current_screen = SCR_FRONT_VIEW;
			break;
	}
}


void d_pressed (void)
{
	switch (current_screen)
	{
		case SCR_GALACTIC_CHART:
		case SCR_SHORT_RANGE:
			show_distance_to_planet();
			break;
		
		case SCR_FRONT_VIEW:
		case SCR_REAR_VIEW:
		case SCR_RIGHT_VIEW:
		case SCR_LEFT_VIEW:
			if (auto_pilot)
				disengage_auto_pilot();
			break;
	}
}


void f_pressed (void)
{
	if ((current_screen == SCR_GALACTIC_CHART) ||
		(current_screen == SCR_SHORT_RANGE))
	{
		find_input = 1;
		find_unknown = false;
		*find_name = '\0';
		gfx_clear_text_area();
		gfx_display_text (16, 340, "Planet Name?");
	}
}


void add_find_char (char letter)
{
	char str[40];
	
	if (strlen (find_name) == 16)
		return;
		
	str[0] = toupper (letter);
	str[1] = '\0';
	strcat (find_name, str);
}


void delete_find_char (void)
{
	char str[40];
	int len;

	len = strlen (find_name);
	if (len == 0)
		return;
		
	find_name[len - 1] = '\0';	
}

void o_pressed()
{
	switch (current_screen)
	{
		case SCR_GALACTIC_CHART:
		case SCR_SHORT_RANGE:
			move_cursor_to_origin();
			break;
	}
}


void auto_dock (void)
{
	struct univ_object ship;

	ship.location.x = 0;
	ship.location.y = 0;
	ship.location.z = 0;
	
	set_init_matrix (ship.rotmat);
	ship.rotmat[2].z = 1;
	ship.rotmat[0].x = -1;
	ship.type = -96;
	ship.velocity = flight_speed;
	ship.acceleration = 0;
	ship.bravery = 0;
	ship.rotz = 0;
	ship.rotx = 0;

	auto_pilot_ship (&ship);

	if (ship.velocity > 22)
		flight_speed = 22;
	else
		flight_speed = ship.velocity;
	
	if (ship.acceleration > 0)
	{
		flight_speed++;
		if (flight_speed > 22)
			flight_speed = 22;
	}

	if (ship.acceleration < 0)
	{
		flight_speed--;
		if (flight_speed < 1)
			flight_speed = 1;
	}	

	if (ship.rotx == 0)
		flight_climb = 0;
	
	if (ship.rotx < 0)
	{
		increase_flight_climb(1.0f);

		if (ship.rotx < -1)
			increase_flight_climb(1.0f);
	}
	
	if (ship.rotx > 0)
	{
		decrease_flight_climb(1.0f);

		if (ship.rotx > 1)
			decrease_flight_climb(1.0f);
	}
	
	if (ship.rotz == 127)
		flight_roll = -14;
	else
	{
		if (ship.rotz == 0)
			flight_roll = 0;

		if (ship.rotz > 0)
		{
			increase_flight_roll(1.0f);

			if (ship.rotz > 1)
				increase_flight_roll(1.0f);
		}
		
		if (ship.rotz < 0)
		{
			decrease_flight_roll(1.0f);

			if (ship.rotz < -1)
				decrease_flight_roll(1.0f);
		}
	}
}


void run_escape_sequence (void)
{
	int i;
	int newship;
	Matrix rotmat;
	
	current_screen = SCR_ESCAPE_POD;
	
	flight_speed = 1;
	flight_roll = 0;
	flight_climb = 0;

	set_init_matrix (rotmat);
	rotmat[2].z = 1.0;
	
	newship = add_new_ship (SHIP_COBRA3, 0, 0, 200, rotmat, -127, -127);
	universe[newship].velocity = 7;
	snd_play_sample (SND_LAUNCH);
	bool redraw = false;

	for (i = 0; i < 90;)
	{
		ALLEGRO_EVENT event;
		al_wait_for_event(event_queue, &event);

		gfx_handle_display_events(event);

		if (event.type == ALLEGRO_EVENT_TIMER)
			redraw = true;

		if (!redraw || !al_is_event_queue_empty(event_queue))
			continue;
		redraw = false;

		if (i == 40)
		{
			universe[newship].flags |= FLG_DEAD;
			snd_play_sample (SND_EXPLODE);
		}

		al_set_clipping_rectangle(GFX_SPACE_OX, GFX_SPACE_OY, GFX_SPACE_WX, GFX_SPACE_WY);
		gfx_clear_display();
		update_starfield();
		update_universe();

		universe[newship].location.x = 0;
		universe[newship].location.y = 0;
		universe[newship].location.z += 2;

		gfx_display_centre_text (358, "Escape pod launched - Ship auto-destuct initiated.", 120, GFX_COL_WHITE);
		
		update_console();
		gfx_update_screen();
		i ++;
	}

	
	while ((ship_count[SHIP_CORIOLIS] == 0) &&
		   (ship_count[SHIP_DODEC] == 0))
	{
		auto_dock();

		if ((abs(flight_roll) < 3) && (abs(flight_climb) < 3))
		{
			for (i = 0; i < MAX_UNIV_OBJECTS; i++)
			{
				if (universe[i].type != 0)
					universe[i].location.z -= 1500;
			}

		}

		warp_stars = 1;
		al_set_clipping_rectangle(GFX_SPACE_OX, GFX_SPACE_OY, GFX_SPACE_WX, GFX_SPACE_WY);
		gfx_clear_display();
		update_starfield();
		update_universe();
		update_console();
		gfx_update_screen();
	}

	abandon_ship();
}

void joystick_low_level(double time_step)
{
	if(joystick == NULL)
		return;

	ALLEGRO_JOYSTICK_STATE joy_state;
	al_get_joystick_state(joystick, &joy_state);

	int n_sticks = al_get_joystick_num_sticks(joystick);
	const float dead_zone = 0.00;
	float vval = 0.0f, hval = 0.0;

	if(n_sticks > config_joy_v_stick)
	{
		int n_axis = al_get_joystick_num_axes(joystick, config_joy_v_stick);
		if (n_axis > config_joy_v_axis)
			vval = joy_state.stick[config_joy_v_stick].axis[config_joy_v_axis];
	}

	if(n_sticks > config_joy_h_stick)
	{
		int n_axis = al_get_joystick_num_axes(joystick, config_joy_h_stick);
		if (n_axis > config_joy_h_axis)
			hval = joy_state.stick[config_joy_h_stick].axis[config_joy_h_axis];
	}

	joy_h = hval;
	joy_v = vval;
	joy_h_in_deadzone = fabs(joy_h) < config_joy_deadzone;
	joy_v_in_deadzone = fabs(joy_v) < config_joy_deadzone;
}

void handle_flight_keys (double time_step)
{	
	kbd_poll_keyboard();

	if (joystick != NULL)
	{	
		joystick_low_level(time_step);

		if(!joy_v_in_deadzone)
		{	
			if (joy_v < 0.0f)
				arrow_up(-joy_v);
			else
				arrow_down(joy_v);
		}
		if(!joy_h_in_deadzone)
		{	
			if (joy_h < 0.0f)
				arrow_left(-joy_h);
			else
				arrow_right(joy_h);
		}

		ALLEGRO_JOYSTICK_STATE joy_state;
		al_get_joystick_state(joystick, &joy_state);

		int n_buttons = al_get_joystick_num_buttons(joystick);
		if (config_joy_b_fire < n_buttons && joy_state.button[config_joy_b_fire])
			kbd_fire_pressed = 1;

		if (config_joy_b_accel < n_buttons && joy_state.button[config_joy_b_accel])
			kbd_inc_speed_pressed = 1;

		if (config_joy_b_brake < n_buttons && joy_state.button[config_joy_b_brake])
			kbd_dec_speed_pressed = 1;

		if (config_joy_b_jump < n_buttons && joy_state.button[config_joy_b_jump])
			kbd_jump_pressed = 1;

	}

	
	if (game_paused)
	{
		if (kbd_resume_pressed)
			game_paused = 0;
		return;
	}
		
	if (kbd_F1_pressed)
	{
		find_input = 0;
		
		if (docked)
			launch_player();
		else
		{
			if (current_screen != SCR_FRONT_VIEW)
			{
				current_screen = SCR_FRONT_VIEW;
				flip_stars();
			}
		}
	}

	if (kbd_F2_pressed)
	{
		find_input = 0;
		
		if (!docked)
		{
			if (current_screen != SCR_REAR_VIEW)
			{
				current_screen = SCR_REAR_VIEW;
				flip_stars();
			}
		}
	}

	if (kbd_F3_pressed)
	{
		find_input = 0;
		
		if (!docked)
		{
			if (current_screen != SCR_LEFT_VIEW)
			{
				current_screen = SCR_LEFT_VIEW;
				flip_stars();
			}
		}
	}

	if (kbd_F4_pressed)
	{
		find_input = 0;
		
		if (docked)
		{
			init_equip_ship ();
			current_screen = SCR_EQUIP_SHIP;
		}
		else
		{
			if (current_screen != SCR_RIGHT_VIEW)
			{
				current_screen = SCR_RIGHT_VIEW;
				flip_stars();
			}
		}
	}

	
	if (kbd_F5_pressed)
	{
		find_input = 0;
		find_unknown = false;
		old_cross_x = -1;
		current_screen = SCR_GALACTIC_CHART;
		calc_cursor_galactic_chart();
	}

	if (kbd_F6_pressed)
	{
		find_input = 0;
		find_unknown = false;
		old_cross_x = -1;
		current_screen = SCR_SHORT_RANGE;
		calc_cursor_short_range_chart();
	}

	if (kbd_F7_pressed)
	{
		find_input = 0;
		current_screen = SCR_PLANET_DATA;
	}

	if (kbd_F8_pressed && (!witchspace))
	{
		find_input = 0;
		init_market_prices ();
		current_screen = SCR_MARKET_PRICES;
	}	

	if (kbd_F9_pressed)
	{
		find_input = 0;
		current_screen = SCR_CMDR_STATUS;
	}

	if (kbd_F10_pressed)
	{
		find_input = 0;
		current_screen = SCR_INVENTORY;
	}
	
	if (kbd_F11_pressed)
	{
		find_input = 0;
		current_screen = SCR_OPTIONS;
		display_options_init();
	}

	if (find_input)
		return;

	if (kbd_y_pressed)
		y_pressed();

	if (kbd_n_pressed)
		n_pressed();

 
	if (kbd_fire_pressed)
	{
		if ((!docked) && (draw_lasers == 0))
			draw_lasers = fire_laser();
	}

	if (kbd_dock_pressed)
	{
		if (!docked && cmdr.docking_computer)
		{
			if (instant_dock)
				engage_docking_computer();
			else
				engage_auto_pilot();
		}
	}

	if (kbd_d_pressed)
		d_pressed();
	
	if (kbd_ecm_pressed)
	{
		if (!docked && cmdr.ecm)
			activate_ecm(1);
	}

	if (kbd_find_pressed)
		f_pressed ();
	
	if (kbd_hyperspace_pressed && (!docked))
	{
		if (kbd_ctrl_pressed)
			start_galactic_hyperspace();
		else
			start_hyperspace();
	}

	if (kbd_jump_pressed && (!docked) && (!witchspace))
	{
		jump_warp();
	}
	
	if (kbd_fire_missile_pressed)
	{
		if (!docked)
			fire_missile();
	}

	if (kbd_origin_pressed)
		o_pressed();

	if (kbd_pause_pressed)
		game_paused = 1;
	
	if (kbd_target_missile_pressed)
	{
		if (!docked)
			arm_missile();		
	}

	if (kbd_unarm_missile_pressed)
	{
		if (!docked)
			unarm_missile();
	}
	
	if (kbd_inc_speed_pressed)
	{
		if (!docked)
		{
			if (flight_speed < myship.max_speed)
				flight_speed++;
		}
	}

	if (kbd_dec_speed_pressed)
	{
		if (!docked)
		{
			if (flight_speed > 1)
				flight_speed--;
		}
	}

	if (kbd_up_pressed)
		arrow_up(1.0f);
	
	if (kbd_down_pressed)
		arrow_down(1.0f);

	if (kbd_left_pressed)
		arrow_left(1.0f);
		
	if (kbd_right_pressed)
		arrow_right(1.0f);
	
	if (kbd_energy_bomb_pressed)
	{
		if ((!docked) && (cmdr.energy_bomb))
		{
			detonate_bomb = 1;
			cmdr.energy_bomb = 0;
		}
	}		

	if (kbd_escape_pressed)
	{
		if ((!docked) && (cmdr.escape_pod) && (!witchspace))
			run_escape_sequence();
	}
}

void handle_typing_keys (const char *keyasc)
{	
	if (strcmp(keyasc, "Return")==0)
	{
		find_input = 0;
		find_unknown = !find_planet_by_name (find_name);
		return;
	}

	if (strcmp(keyasc, "BackSpace")==0)
	{
		delete_find_char();
		return;
	}

	if (strlen(keyasc)==1 && isalpha(keyasc[0]))
		add_find_char (keyasc[0]);

	return;		
}

void set_commander_name (char *path)
{
	char *fname = NULL, *cname = NULL;
	
	ALLEGRO_PATH *ap = al_create_path(path);
	fname = al_get_path_filename (ap);
	cname = cmdr.name;
	
	for (int i = 0; i < 31; i++)
	{
		if (!isalnum(*fname))
			break;
		
		*cname++ = toupper(*fname++);
	}	

	*cname = '\0';

	al_destroy_path(ap);
}


void save_commander_screen (void)
{
	char path[255] = "";
	int okay = true;
	int rv = 0;
	
	current_screen = SCR_SAVE_CMDR;

	gfx_clear_display();
	gfx_display_centre_text (10, "SAVE COMMANDER", 140, GFX_COL_GOLD);
	gfx_draw_line (0, SCALE_Y(36), GFX_VIEW_WX-1, SCALE_Y(36));
	gfx_update_screen();
	
	strncpy (path, cmdr.name, 250);
	strcat (path, ".nkc");
	
	okay = gfx_request_file ("Save Commander", path, "*.nkc", true);
	
	if (!okay)
	{
		display_options();
		return;
	}

	strncpy(save_path, path, 254);
	rv = save_commander_file (save_path);

	if (rv)
	{
		gfx_display_centre_text (175, "Error Saving Commander!", 140, GFX_COL_GOLD);
		return;
	}
	
	current_screen = SCR_CMDR_SAVED;

	set_commander_name (path);
	saved_cmdr = cmdr;
	saved_cmdr.ship_x = docked_planet.d;
	saved_cmdr.ship_y = docked_planet.b;
}


void load_commander_screen (void)
{
	char path[255];
	int rv;

	gfx_clear_display();
	gfx_display_centre_text (10, "LOAD COMMANDER", 140, GFX_COL_GOLD);
	gfx_draw_line (0, SCALE_Y(36), GFX_VIEW_WX-1, SCALE_Y(36));
	gfx_update_screen();
	
	if(strlen(save_path) == 0)
		strncpy (path, "jameson.nkc", 254);
	else
		strncpy (path, save_path, 254);
	
	rv = gfx_request_file ("Load Commander", path, "*.nkc", false);

	if (rv == 0)
		return;

	rv = load_commander_file (path);
	strncpy(save_path, path, 254);

	if (rv)
	{
		saved_cmdr = cmdr;
		gfx_display_centre_text (175, "Error Loading Commander!", 140, GFX_COL_GOLD);
		gfx_display_centre_text (200, "Press any key to continue.", 140, GFX_COL_GOLD);
		gfx_update_screen();

		while(true)
		{
			ALLEGRO_EVENT event;
			al_wait_for_event(event_queue, &event);

			gfx_handle_display_events(event);

			if (event.type == ALLEGRO_EVENT_KEY_CHAR)
				break;
		}

		return;
	}
	
	restore_saved_commander();
	set_commander_name (path);
	saved_cmdr = cmdr;
	
	//update_console();
}



void run_first_intro_screen (void)
{
	current_screen = SCR_INTRO_ONE;

	snd_play_midi (SND_ELITE_THEME, true);

	initialise_intro1();
	bool redraw = false;

	while (!finish)
	{
		ALLEGRO_EVENT event;
		al_wait_for_event(event_queue, &event);

		gfx_handle_display_events(event);

		if (event.type != ALLEGRO_EVENT_TIMER 
			&& event.type != ALLEGRO_EVENT_JOYSTICK_BUTTON_DOWN)
			continue;

		if (event.type == ALLEGRO_EVENT_TIMER)
			redraw = true;

		if (redraw && al_is_event_queue_empty(event_queue))
		{
			redraw = false;
			int64_t timer_count = event.timer.count;
			double time_step = (timer_count - prev_timer_count) * al_get_timer_speed(timer);
			prev_timer_count = timer_count;

			gfx_set_target_backbuffer();
			gfx_clear_display();

			gfx_set_clip_region (0, 0, GFX_VIEW_WX, GFX_VIEW_WY);
			gfx_draw_scanner();
			//gfx_draw_cockpit();

			update_intro1();

			gfx_update_screen();

			joystick_low_level(time_step);
		}

		kbd_poll_keyboard();

		if (event.type == ALLEGRO_EVENT_JOYSTICK_BUTTON_DOWN)
		{
			if(event.joystick.button == 0)
				kbd_y_pressed = 1;
			if(event.joystick.button == 1)
				kbd_n_pressed = 1;
		}

		if (kbd_y_pressed)
		{
			snd_stop_midi();	
			load_commander_screen();
			break;
		}
		
		if (kbd_n_pressed)
		{ 
			snd_stop_midi();	
			break;
		}
	} 

}



void run_second_intro_screen (void)
{
	current_screen = SCR_INTRO_TWO;
	
	snd_play_midi (SND_BLUE_DANUBE, true);
	
	initialise_intro2();

	flight_speed = 3;
	flight_roll = 0;
	flight_climb = 0;
	bool redraw = false;

	while (!finish)
	{
		ALLEGRO_EVENT event;
		al_wait_for_event(event_queue, &event);

		gfx_handle_display_events(event);

		if (event.type != ALLEGRO_EVENT_TIMER 
			&& event.type != ALLEGRO_EVENT_JOYSTICK_BUTTON_DOWN)
			continue;

		if (event.type == ALLEGRO_EVENT_TIMER)
			redraw = true;
	
		if (redraw && al_is_event_queue_empty(event_queue))
		{
			int64_t timer_count = event.timer.count;
			double time_step = (timer_count - prev_timer_count) * al_get_timer_speed(timer);
			prev_timer_count = timer_count;

			gfx_set_target_backbuffer();
			gfx_clear_display();

			gfx_set_clip_region (0, 0, GFX_VIEW_WX, GFX_VIEW_WY);
			gfx_draw_scanner();
			//gfx_draw_cockpit();

			update_intro2();

			gfx_update_screen();

			joystick_low_level(time_step);
		}

		kbd_poll_keyboard();

		if (event.type == ALLEGRO_EVENT_JOYSTICK_BUTTON_DOWN)
		{
			if(event.joystick.button == 0)
				kbd_space_pressed = 1;
		}

		if (kbd_space_pressed) 
			break;
	} 

	snd_stop_midi();
}



/*
 * Draw the game over sequence. 
 */

void run_game_over_screen()
{
	int i;
	int newship;
	Matrix rotmat;
	int type;
	
	current_screen = SCR_GAME_OVER;
	al_set_clipping_rectangle(GFX_SPACE_OX, GFX_SPACE_OY, GFX_SPACE_WX, GFX_SPACE_WY);
	
	flight_speed = 6;
	flight_roll = 0;
	flight_climb = 0;
	clear_universe();

	set_init_matrix (rotmat);

	newship = add_new_ship (SHIP_COBRA3, 0, 0, -400, rotmat, 0, 0);
	universe[newship].flags |= FLG_DEAD;

	for (i = 0; i < 5; i++)
	{
		type = (rand255() & 1) ? SHIP_CARGO : SHIP_ALLOY;
		newship = add_new_ship (type, (rand255() & 63) - 32,
								(rand255() & 63) - 32, -400, rotmat, 0, 0);
		universe[newship].rotz = ((rand255() * 2) & 255) - 128;
		universe[newship].rotx = ((rand255() * 2) & 255) - 128;
		universe[newship].velocity = rand255() & 15;
	}
	
	i=0;
	bool redraw = false;
	while (i < 100)
	{
		ALLEGRO_EVENT event;
		al_wait_for_event(event_queue, &event);

		gfx_handle_display_events(event);

		if (event.type == ALLEGRO_EVENT_TIMER)
			redraw = true;

		if (!redraw || !al_is_event_queue_empty(event_queue))
			continue;
		redraw = false;

		gfx_clear_display();
		update_starfield();
		update_universe();
		gfx_display_centre_text (190, "GAME OVER", 140, GFX_COL_GOLD);
		gfx_update_screen();

		i++;
	}
}




/*
 * Draw a break pattern (for launching, docking and hyperspacing).
 * Just draw a very simple one for the moment.
 */

void display_break_pattern (void)
{
	ALLEGRO_COLOR white = al_map_rgb(255, 255, 255);

	int i=0;	
	bool redraw = false;
	while (i < 20)
	{
		ALLEGRO_EVENT event;
		al_wait_for_event(event_queue, &event);

		gfx_handle_display_events(event);

		if (event.type == ALLEGRO_EVENT_TIMER)
			redraw = true;

		if (!redraw || !al_is_event_queue_empty(event_queue))
			continue;
		redraw = false;

		al_set_clipping_rectangle(GFX_SPACE_OX, GFX_SPACE_OY, GFX_SPACE_WX, GFX_SPACE_WY);
		gfx_clear_display();

		for (int j = 0; j < i; j++)
		{
			gfx_draw_circle (SCALE_X(256), SCALE_Y(192), SCALE_X(30 + j * 15), white);
		}

		update_console();
		gfx_update_screen();
	
		i++;
	}	

	if (docked)
	{

		if(strlen(save_path) > 0)
		{
			//Do autosave
			ALLEGRO_PATH *pth = al_create_path(save_path);
			al_set_path_filename(pth, "autosave.nkc");

			save_commander_file (al_path_cstr(pth, ALLEGRO_NATIVE_PATH_SEP));

			al_destroy_path(pth);
		}

		check_mission_brief();
		current_screen = SCR_CMDR_STATUS;
	}
	else
		current_screen = SCR_FRONT_VIEW;
}


void info_message (char *message)
{
	strcpy (message_string, message);
	message_count = 37;
//	snd_play_sample (SND_BEEP);
}






void initialise_allegro (void)
{
	if(!al_init())
	{
		fprintf(stderr, "failed to initialize allegro!\n");
		return -1;
	}
	event_queue = al_create_event_queue();

	al_install_keyboard(); 
	al_register_event_source(event_queue, al_get_keyboard_event_source());

	/* Install a timer to regulate the speed of the game... */
	double timer_period = (double)speed_cap / 1000.0;
	timer = al_create_timer(timer_period);
	if(!timer) {
		printf("failed to create timer!\n");
		return 1;
	}
 
	al_register_event_source(event_queue, al_get_timer_event_source(timer));

	al_install_mouse();
	al_init_image_addon();
	al_init_primitives_addon();

	have_joystick = 0;
	
	if (al_install_joystick())
	{
		al_register_event_source(event_queue, al_get_joystick_event_source()); 

		int joy_num = config_joystick_num;
		have_joystick = (al_get_num_joysticks() > config_joystick_num);
		if(have_joystick)
		{
			joystick = al_get_joystick(config_joystick_num);
		}

		//Fall back to first joystick if we can't find user selected
		if(joystick == NULL)
		{
			have_joystick = (al_get_num_joysticks() > 0);
			if(have_joystick)
			{
				joystick = al_get_joystick(0);
				joy_num = 0;
			}
		}

		if(joystick != NULL)
			printf("Connected to joystick %d %s\n", joy_num, al_get_joystick_name(joystick));
	}
	
	al_start_timer(timer);
}



int main(int argc, char *argv[])
{
	if(argc>=2)
		*config_filename = argv[1];
	read_config_file(*config_filename);

	initialise_allegro();

	if (gfx_graphics_startup() == 1)
	{
		return 1;
	}
	
	/* Start the sound system... */
	snd_sound_startup();

	/* Do any setup necessary for the keyboard... */
	kbd_keyboard_startup();
	
	finish = 0;
	auto_pilot = 0;
	strncpy(save_path, "", 254);
	
	while (!finish)
	{
		game_over = 0;	
		initialise_game();
		dock_player();

		current_screen = SCR_FRONT_VIEW;
		run_first_intro_screen();
		run_second_intro_screen();

		old_cross_x = -1;
		old_cross_y = -1;

		dock_player ();
		current_screen = SCR_CMDR_STATUS;
		bool redraw = false;

		while (!game_over)
		{
			ALLEGRO_EVENT event;
			al_wait_for_event(event_queue, &event);

			gfx_handle_display_events(event);

			if (event.type == ALLEGRO_EVENT_KEY_CHAR)
			{
				if (find_input)
					handle_typing_keys(al_keycode_to_name(event.keyboard.keycode));

				else if(strcmp(al_keycode_to_name(event.keyboard.keycode), "Return")==0)
					return_pressed();
			}

			if (event.type == ALLEGRO_EVENT_TIMER)
				redraw = true;

			if (!redraw || !al_is_event_queue_empty(event_queue))
				continue;

			int64_t timer_count = event.timer.count;
			double time_step = (timer_count - prev_timer_count) * al_get_timer_speed(timer);
			prev_timer_count = timer_count;
			redraw = false;

			snd_update_sound();
			al_set_clipping_rectangle(GFX_SPACE_OX, GFX_SPACE_OY, GFX_SPACE_WX, GFX_SPACE_WY);

			rolling = 0;
			climbing = 0;

			handle_flight_keys (time_step);
			gfx_set_target_backbuffer();
			gfx_clear_display();

			if (game_paused)
				continue;
				
			if (message_count > 0)
				message_count--;

			if (!rolling)
			{
				if (flight_roll > 0.0f)
				{
					float drag = flight_roll;
					if(drag > 1.0f) drag = 1.0f;
					decrease_flight_roll(drag);
				}

				if (flight_roll < 0.0f)
				{
					float drag = -flight_roll;
					if(drag > 1.0f) drag = 1.0f;
					increase_flight_roll(1.0f);
				}
			}

			if (!climbing)
			{
				if (flight_climb > 0.0f)
				{
					float drag = flight_climb;
					if(drag > 1.0f) drag = 1.0f;
					decrease_flight_climb(drag);
				}

				if (flight_climb < 0.0f)
				{
					float drag = -flight_climb;
					if(drag > 1.0f) drag = 1.0f;
					increase_flight_climb(drag);
				}
			}

			if (!docked)
			{
		
				if ((current_screen == SCR_FRONT_VIEW) || (current_screen == SCR_REAR_VIEW) ||
					(current_screen == SCR_LEFT_VIEW) || (current_screen == SCR_RIGHT_VIEW) ||
					(current_screen == SCR_INTRO_ONE) || (current_screen == SCR_INTRO_TWO) ||
					(current_screen == SCR_GAME_OVER))
				{
					update_starfield();
				}

				if (auto_pilot)
				{
					auto_dock();
					if ((mycount & 127) == 0)
						info_message ("Docking Computers On");
				}

				update_universe ();

				if (docked)
				{
					update_console();
					continue;
				}

				if ((current_screen == SCR_FRONT_VIEW) || (current_screen == SCR_REAR_VIEW) ||
					(current_screen == SCR_LEFT_VIEW) || (current_screen == SCR_RIGHT_VIEW))
				{
					if (draw_lasers)
					{
						draw_laser_lines();
						draw_lasers--;
					}
					
					draw_laser_sights();
				}

				if (message_count > 0)
					gfx_display_centre_text (358, message_string, 120, GFX_COL_WHITE);
					
				if (hyper_ready)
				{
					display_hyper_status();
					if ((mycount & 3) == 0)
					{
						//This can change the draw target
						countdown_hyperspace();
						gfx_set_target_backbuffer();
					}
				}
			
				mycount--;
				if (mycount < 0)
					mycount = 255;

				if ((mycount & 7) == 0)
					regenerate_shields();

				if ((mycount & 31) == 10)
				{
					if (energy < 50)
					{
						info_message ("ENERGY LOW");
						snd_play_sample (SND_BEEP);
					}

					update_altitude();
				}
				
				if ((mycount & 31) == 20)
					update_cabin_temp();
					
				if ((mycount == 0) && (!witchspace))
					random_encounter();
					
				cool_laser();				
				time_ecm();
			}

			if (current_screen == SCR_BREAK_PATTERN)
				display_break_pattern();

			else if(current_screen == SCR_GALACTIC_CHART || current_screen == SCR_SHORT_RANGE)
			{
				if(current_screen == SCR_GALACTIC_CHART)
					display_galactic_chart();
				else if(current_screen == SCR_SHORT_RANGE)
					display_short_range_chart();

				if (cross_timer > 0)
				{
					cross_timer--;
					find_unknown = false;

					if (cross_timer == 0)
						snap_cursor_to_planet();
				}
				if (cross_timer == 0 && !find_input && !find_unknown)
					show_distance_to_planet();
				if (find_input)
				{
					char str[40];
					sprintf (str, "Planet Name? %s", find_name);		
					gfx_clear_text_area ();
					gfx_display_text(16, 340, str);
				}
				if (find_unknown)
				{
					gfx_clear_text_area();
					gfx_display_text (16, 340, "Unknown Planet");
				}
				
				draw_cross (cross_x, cross_y);
				old_cross_x = cross_x;
				old_cross_y = cross_y;

			}

			else if(current_screen == SCR_EQUIP_SHIP)
				equip_ship();

			else if(current_screen == SCR_PLANET_DATA)
				display_data_on_planet();

			else if(current_screen == SCR_MARKET_PRICES)
				display_market_prices();

			else if(current_screen == SCR_CMDR_STATUS)
				display_commander_status();

			else if(current_screen == SCR_INVENTORY)
				display_inventory();

			else if(current_screen == SCR_OPTIONS)
				display_options();

			else if(current_screen == SCR_SETTINGS)
				game_settings_screen();

			else if(current_screen == SCR_CMDR_SAVED)
			{
				gfx_draw_line (0, SCALE_Y(36), GFX_VIEW_WX-1, SCALE_Y(36));
				gfx_display_centre_text (175, "Commander Saved.", 140, GFX_COL_GOLD);
			}

			else if(current_screen == SCR_QUIT)
				quit_screen();

			update_console();

			gfx_update_screen();

		} //End of main loop
		
		if (!finish)		
			run_game_over_screen();
	}

	snd_sound_shutdown();
	
	gfx_graphics_shutdown ();

	al_destroy_timer(timer);

	al_destroy_event_queue(event_queue);
	
	return 0;
}

END_OF_MAIN();
